<?php
namespace ZB;
class Builder {
    
    public $buildedBootstrap = '';
    public $buildedClasses = '';
    public $buildedNSClasses = '';
    
    public function cleanup()
    {
        @unlink(ROOT.'builded_classes.php');
        @unlink(ROOT.'builded_nsclasses.php');
        @unlink(ROOT.'builded_bootstrap.php');        
    }
    function prepareBootstrap()
    {
        $this->buildedBootstrap .= $this->closePHPTag( file_get_contents(ROOT.'bootstrap.php') );
    }
    private function closePHPTag($text)
    {
        preg_match('/(\?>){1}$/is',$text,$matches);
        if (!isset($matches[1])){
            $text.="?>";
        }
        return $text;
    }
    function prepareClass($classname){
        $body = trim( autoloader($classname,1) );
        $body = $this->closePHPTag($body)."\n";
        if (strpos($classname,"\\")!==false){
            $this->buildedNSClasses .= $body;
        }else{
            $this->buildedClasses .= $body;
        }
    }
    function addPHPCode($php){
        $this->buildedBootstrap .= "<?php ".$php." ?>\n";
    }
    function addPHPFile($filename){
        $body = trim( file_get_contents($filename) );
        $this->buildedBootstrap .= $this->closePHPTag($body)."\n";
    }
    function build(){
        if (!empty($this->buildedClasses)){
            file_put_contents(ROOT.'builded_classes.php',$this->buildedClasses);
            $this->buildedBootstrap = "<?php require 'builded_classes.php';?>\n".$this->buildedBootstrap;
        }
        if (!empty($this->buildedNSClasses)){
            file_put_contents(ROOT.'builded_nsclasses.php',$this->buildedNSClasses);
            $this->buildedBootstrap = "<?php require 'builded_nsclasses.php';?>\n".$this->buildedBootstrap;
        }
        file_put_contents(ROOT.'builded_bootstrap.php',$this->buildedBootstrap);    
    }
}
