<?php
namespace ZB;

class Template
{
    /**
     * шаблон
     */
    private $tpl=null;
    
    /**
     * переменные шаблона
     */
    private $data = array();
    
    private $tmp = array();
    
    private $layout;
    
    function __construct($layout=null){
        $this->layout = $layout;
    }
    
    function setLayout($layout){
        $this->layout = $layout;
    }
    
    function __set($var, $val)
    {
        $this->data[$var]=$val;
    }

    function __isset($var)
    {
        return isset( $this->data[$var] );
    }
    
    function __get($var)
    {
        return $this->data[$var];
    }
    
    function addtplfile($tpl,$folder=null)
    {
        $tpl = str_replace(['/','\\'], DIRECTORY_SEPARATOR, $tpl);
        if(strpos($tpl,'.'.DIRECTORY_SEPARATOR)===0)
        {
          $tPath = explode(DIRECTORY_SEPARATOR, $this->tmp['tpl']);
          array_pop($tPath);
          $tpl = implode( DIRECTORY_SEPARATOR,$tPath) . DIRECTORY_SEPARATOR . substr($tpl, 2) ;
        }
        echo $this->parse($tpl,$folder);
    }
    
    function pageContent(){
        $this->addtplfile($this->tpl);    
    }
    /**
     * обертка для parse. просто сразу выводит, что получилось
     * @param string $tpl - имя шаблона
     * @uses parse
     * @return void
     */
    function output($tpl, $folder = null)
    {
        echo $this->parse($tpl, $folder);
    }
    
    /**
     * парсинг шаблона (необходимо передать шаблон, если не был передан ранее)
     * @param string $tpl - имя шаблона
     * @return string результат парсинга
     */
    function parse($tpl,$folder=null)
    {
        $tpl = str_replace(['/','\\'], DIRECTORY_SEPARATOR, $tpl);
        $previousTpl = isset($this->tmp['tpl']) ? $this->tmp['tpl'] : null;
        if ($this->layout){
            $this->tpl = $tpl;
            $tpl = $this->layout;
            $this->layout = false;
        }
        if ($folder && is_file( ROOT.'tpl'.DIRECTORY_SEPARATOR.$folder.DIRECTORY_SEPARATOR.$tpl.'.tpl' ) ){
            $tpl = $folder.DIRECTORY_SEPARATOR.$tpl;
        }
        
        unset ($folder);
        $this->tmp['tpl'] = $tpl;
        unset($tpl);
        extract($this->data);
        ob_start();
        try
        {
            if (!is_file(ROOT.'tpl'.DIRECTORY_SEPARATOR.$this->tmp['tpl'].'.tpl')){
                throw new \Exception('Try to include missed template '. $this->tmp['tpl'].'.tpl in '.($previousTpl ? $previousTpl.'.tpl': 'root'));
            }
            include ROOT.'tpl'.DIRECTORY_SEPARATOR.$this->tmp['tpl'].'.tpl';
        }
        catch (\Exception $e)
        {
            ob_end_clean();
            throw $e;
        }
        $data = ob_get_clean();
        $this->tmp['tpl'] = $previousTpl;
        return $data;
    }
}