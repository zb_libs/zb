<?php
namespace ZB;

class Config{
    
    static $files=array();
    protected static $path = '';
    protected static $localPath;
    static $data=array();
    
    public static function get($var)
    {
        $path= explode('.',$var);
        if (!in_array($path[0],self::$files)){
            self::$files[]=$path[0];
            $localdata = [];
            if ( is_file(static::getLocalPath() . $path[0] . '.conf') )
            {
                $localdata = include(static::getLocalPath() . $path[0] . '.conf');
            }
            if ( is_file(static::$path.$path[0].'.conf') ){
                self::$data[$path[0]] = array_replace_recursive( include(static::$path.$path[0].'.conf'), $localdata );
            }else{
                self::$data[$path[0]] = $localdata;
            }
            
        }
        $config = self::$data[$path[0]];
        unset($path[0]);
        foreach ($path as $node)
        {
            if (!isset($config[$node])){
                return null;
            }
            $config = $config[$node];
        }
        return $config;
    }
    
    public static function export()
    {
        foreach ( glob(static::$path."*.conf") as $filename ) {
            static::get( substr(basename($filename),0,-5));
        }
        return ['data'=>static::$data, 'files'=>static::$files];
    }
    public static function import($data, $files)
    {
        static::$data = $data;
        static::$files = $files;
    }
    
    public static function setPath($path)
    {
        static::$path = $path;
    }
    
    public static function getPath()
    {
        return static::$path;
    }
    
    public static function setLocalPath($path)
    {
        static::$localPath = $path;
    }
    public static function getLocalPath()
    {
        if (!is_null(static::$localPath)){
            return static::$localPath;
        }
        return static::$path.'loc'.DIRECTORY_SEPARATOR;
    }
}