<?php

namespace ZB;

class Precontroller
{
    private $data = [];
    
    function import($data){
        $obj = $this;
        $this->data = [];
        array_walk($data, function($val, $key) use ($obj)
        {
            $this->$key = $val;
        });
    }
    public function __get($var)
    {
        return $this->data[$var];
    }
    public function __set($var, $val)
    {
        $this->data[$var]=$val;    
    }

    public function __isset($var)
    {
        return isset( $this->data[$var] );
    }

    function export()
    {
        return $this->data;
    }
}