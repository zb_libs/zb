<?php

namespace ZB;

/**
 * @package ZB
 * @access public
 */
class Request {
    /**
     * Request URI
     * @var string
     */
    private $uri;
    
    /**
     * URL path
     * @var string
     */
    private $path;
    
    /**
     * HTTP method
     * @var string
     */
    private $method;
    
    /**
     * Ascertained controller name
     * @var string
     */
    private $controller;
    
    /**
     * Ascertained action name
     * @var string
     */
    private $action;
    
    /**
     * Ascertained params
     * @var Array
     */
    private $params = [];
    
    /**
     * $_GET
     * @var Array
     */
    private $get = [];
    
    /**
     * $_POST
     * @var Array
     */
    private $post = [];
      
    /**
     * params from cli
     * @var Array
     */
    private $cli = [];
    
    /**
     * http headers
     */
     private $headers = [];
     
    /**
     * $_FILES 
     * @var Array
     */
    private $files;
    
    /**
     * Real user's IP v4 adress
     * @var string
     */
    private $userIP;
    
    /**
     * User Agent
     * @var string
     */
    private $userAgent;
    
    
    /**
     * Defines route for request 
     * @param mixed $controllerName
     * @param mixed $actionName
     * @param mixed $params
     */
    public function defineRoute($controllerName, $actionName, $params)
    {
        if (!is_null($this->controller) || !is_null($this->action) ){
            throw new \Exception('Route already defined');
        }
        $this->controller = $controllerName;
        $this->action = $actionName;
        $this->params = $params;
    }
    
    /**
     * Tries to import properties from HTTP Environment
     */
    public function loadDataFromHTTP()
    {
        $this->headers = $this->getAllHeaders();
        $this->uri = isset($_SERVER['REQUEST_URI'])? $_SERVER['REQUEST_URI'] : '/';
        $this->path = parse_url($this->uri, PHP_URL_PATH);
        $this->method = ( ! empty($_SERVER['REQUEST_METHOD']))? strtolower($_SERVER['REQUEST_METHOD']) : 'NA';
        $this->get = isset($_GET)? $_GET : [];
        $this->post = ($this->method=='post') ? $this->parsePOST() : [];
        if ($this->method=='put' || $this->method=='patch')
        {
            $this->post = $this->parsePhpInput();
        }
        $this->files = isset($_FILES)? $_FILES : [];
        $ip = isset($_SERVER['REMOTE_ADDR'])? $_SERVER['REMOTE_ADDR'] : null;
        $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR'])? $_SERVER['HTTP_X_FORWARDED_FOR'] : $ip ;
        $this->userIP = $ip;
        $this->userAgent = isset($_SERVER['HTTP_USER_AGENT'])? $_SERVER['HTTP_USER_AGENT'] : null ;
    }
        
    public function loadDataFromCLI()
    {
        $options = getopt("",["path:",'params:']);
        $this->path = isset($options['path']) ? $options['path'] : '/';
        $this->method = 'cli';
        $cliParams = isset($options['params']) ? $options['params'] : '';
        $this->cli = !empty($data) ? $data : [];
        $this->userIP = '127.0.0.1';
        $this->userAgent = 'cli';
    }
    
    private function parsePOST()
    {
        $post = isset($_POST)? $_POST : [];
        if (strpos($this->headers('content-type'),'application/json')!==false){
            $body = file_get_contents('php://input');
            $json = json_decode($body);
            if ($json){
                $post = (array)$json;
            }
        }
        return $post;
    }
    
    private function parsePhpInput()
    {
        $putdata = fopen("php://input", "r");
        $raw_data = '';
        while ($chunk = fread($putdata, 1024))
        {
            $raw_data .= $chunk;
        }
        fclose($putdata);
        if (strpos($this->headers('content-type'),';')!==false){
            $contentType = substr($this->headers('content-type'), 0, strpos($this->headers('content-type'),';'));
        }else{
            $contentType = $this->headers('content-type');
        }
        if (strpos($this->headers('content-type'),'application/x-www-form-urlencoded')!==false)
        {
            parse_str($raw_data,$data);
            return $data;
        }
        if (strpos($this->headers('content-type'),'application/json')!==false)
        {
            $json = json_decode($raw_data);
            if ($json){
                return (array)$json;
            }
            return [];
        }
        if (strpos($this->headers('content-type'),'multipart/form-data')===false)
        {
            return [];
        }
        $raw_data = ltrim($raw_data);
        $NLSymbol = strpos($raw_data,"\r\n")!==false ? "\r\n":"\n";
        $dblNLSymbol = $NLSymbol.$NLSymbol;
        $boundary = substr($raw_data, 0, strpos($raw_data, $NLSymbol));

        if(empty($boundary)){
            return [];
        }
        $parts = array_slice(explode($boundary, $raw_data), 1);
        $data = array();
        foreach ($parts as $part) {
            if ($part == "--".$NLSymbol) break;
            $part = ltrim($part, $NLSymbol);
            list($raw_headers, $body) = explode($dblNLSymbol, $part, 2);
            $body = substr($body, 0, - strlen($NLSymbol));
            $raw_headers = explode($NLSymbol, $raw_headers);
            $headers = array();
            foreach ($raw_headers as $header) {
                if(strpos($header, ':'))
                {
                    list($name, $value) = explode(':', $header);
                    $headers[strtolower($name)] = ltrim($value, ' ');
                }
            }
            if (isset($headers['content-disposition'])) {
                $filename = null;
                $tmp_name = null;
                preg_match(
                    '/^(.+); *name="?([^";]+)"?(; *filename="?([^";]+)"?)?/',
                    $headers['content-disposition'],
                    $matches
                );
                list(, $type, $name) = $matches;
                //Parse File
                if( isset($matches[4]) )
                {
                    //if labeled the same as previous, skip
                    if( isset( $_FILES[ $matches[ 2 ] ] ) )
                    {
                        continue;
                    }
                    $filename = $matches[4];
                    $filename_parts = pathinfo( $filename );
                    $tmp_name = tempnam( ini_get('upload_tmp_dir'), $filename_parts['filename']);
                    //populate $_FILES with information, size may be off in multibyte situation
                    $_FILES[ $matches[ 2 ] ] = array(
                        'error'=>0,
                        'name'=>$filename,
                        'tmp_name'=>$tmp_name,
                        'size'=>mb_strlen( $body ),
                        'type'=>$value
                    );
                    file_put_contents($tmp_name, $body);
                }
                else
                {
                    $data[$name] = str_replace(["\r","\r\n","\n"],"",$body);
                }
            }
        }
        return $data;
    }
    
    /**
     * Setting object properties. Every property can be setted only once. 
     * @param array $data
     */
    public function importData($data){
        foreach ($data as $key => $value){
            if (empty($this->$key)){
                $this->$key = $value;
            }
        }
    }
    
    /**
     * Request uri
     * @return string
     */
    public function uri() {
        return $this->uri;
    }
    
    /**
     * Request path from url
     * @return string
     */
    public function path() {
        return $this->path;
    }
    
    /**
     * HTTP method
     * @return string
     */
    public function method() {
        return $this->method;
    }
    
    /**
     * Ascertained controller name
     * @return string
     */
    public function controller(){
        return $this->controller;
    }
    
    /**
     * Ascertained action name
     * @return string
     */
    public function action(){
        return $this->action;
    }
    
    private function getValue($property, $key, $default)
    {
        if (is_null($key)){
            return (isset($this->$property)) ? $this->$property : false;
        }
        $array = (isset($this->$property)) ? $this->$property : [];
        return ( isset($array[$key]) && $array[$key] != '' ) ? $array[$key] : $default ;
    }
    /**
     * Wrapper for $_GET
     * @param string $key key in $_GET array. If null, function will return whole $_GET
     * @param mixed $default default value to return in case there is no $key in $_GET
     * @return mixed
     */
    public function get($key=null, $default=null) {
        return $this->getValue('get', $key, $default); 
    }
    
    /**
     * Wrapper for $_POST
     * @param string $key key in $_POST array. If null, function will return whole $_POST
     * @param mixed $default default value to return in case there is no $key in $_POST
     * @return mixed
     */
    public function post($key=null, $default=null) {
        return $this->getValue('post', $key, $default);
    }
    /**
     * Wrapper for $_FILES
     * @param string $key key in $_FILES array. If null, function will return whole $_FILES
     * @param mixed $default default value to return in case there is no $key in $_FILES
     * @return mixed
     */
    public function files($key=null, $default=null) {
        return $this->getValue('files', $key, $default);
    }
    /**
     * Wrapper for getallheaders function. In lower case.
     * @param string $key header name. If null, function will return all headers
     * @param mixed $default default value to return in case there is no $key in headers
     * @return mixed
     */
    public function headers($key=null, $default=null) {
        return $this->getValue('headers', $key, $default);
    }
    /**
     * Returns variables from the ascertained parameters of the request 
     * @param string $key parameter name. If null, function will return all ascertained parameters of the request 
     * @param mixed $default default value to return in case there is no required $key in the ascertained parameters of the request 
     * @return mixed
     */
    public function params($key=null, $default=null) {
        return $this->getValue('params', $key, $default);
    }
    
    /**
     * Ascertained user IP. Including  behind proxy
     * @return string
     */
    public function userIP(){
        return $this->userIP;
    }

    /**
     * type of request
     * @return bool
     */
    public function isAjax(){
        return ( strpos($this->headers('accept'), 'application/json') !==false);
    }
    
    /**
     * User Agent as is
     * @return string
     */
    public function userAgent(){
        return $this->userAgent;
    }
    
    private function getAllHeaders() {
        if (function_exists('getallheaders')) {
            $rawHeaders = getallheaders();
            $headers = [];
            foreach($rawHeaders as $key => $value){
                $headers[strtolower($key)]=$value;
            }
            return $headers;
        } else {
            $headers = [];
            foreach ($_SERVER as $key => $value) {
                if (strpos($key, 'HTTP_') === 0) {
                    $name = implode('-', array_map(function($v) {
                        return strtolower($v);
                    }, explode('_', substr($key, 5))));
                    $headers[$name] = $value;
                }
            }
            return $headers;
        }
    }
}