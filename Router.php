<?php

namespace ZB;

class Router  {
    
    private $routes;
    private $defaultController;
    private $defaultOptionsController;
    private $precontroller;
    private $defaultPrecontrollers;
    private $path;
    private $isConfigPrepared;
    
    public function __construct($path, $routingConfig, $isConfigPrepared = false) {
        $this->path = $path;
        $this->isConfigPrepared = $isConfigPrepared;
        $this->routes = $routingConfig;
    }
    public function setDefaultController($defaultController)
    {
        $this->defaultController = $this->parseClassController($defaultController);
    }
    public function setDefaultOptionsController($defaultOptionsController)
    {
        $this->defaultOptionsController = $this->parseClassController($defaultOptionsController, 'options');
    }
    public function setPrecontroller(Precontroller $object, array $methods)
    {
        $this->precontroller = $object;
        $this->defaultPrecontrollers = $methods;
    }
    private function parseClassController($classController, $method='get'){
        if (strpos($classController,':')){
            $parts = explode(':',$classController);
            $controller = $parts[0];
            $action = $parts[1];
        }else{
            $controller = '';
            $action = $classController.'.'.$method.'.php';
        }
        return ['controller'=>$controller,'action'=>$action];
    }
    public static function prepareRoutes(array $routes) {
        $routing = [];
        foreach ($routes as $path=>$route)
        {
            $pieces = explode('/',$path);
            
            $pieces = array_filter($pieces,function($piece){
                return (trim($piece)!='');
            });
            
            $base = array_shift($pieces);
            if (strstr($base,':')!==false){
                array_unshift($pieces,$base);
                $base='';
            }
            if (empty($routing[$base])){
                $routing[$base]=[];
            }
            $basePath = [$base];
            $paramsPeases = [];
           
            $routeFullPath = &$routing[$base];
            foreach ($pieces as $piece){
                if (strstr($piece,'$')===false){
                    if (empty($routeFullPath['sub'])){
                        $routeFullPath['sub']=[$piece=>[]];
                    }else if (empty($routeFullPath['sub'][$piece])){
                        $routeFullPath['sub'][$piece]=[];
                    }
                    $routeFullPath = &$routeFullPath['sub'][$piece];
                }else{
                    $paramsPeases[] = $piece;
                }
            }
            foreach ($paramsPeases as $key=>$piece)
            {    
                $route = str_replace($piece,'<param'.$key.'>',$route);
                $paramsPeases[$key]=str_replace('$','',$piece);
            
            }
            $routeData = ['controller'=>$route,'params'=>$paramsPeases];
            
            $routeFullPath[count($paramsPeases)] = $routeData;
        }
        return $routing;
    }
    /**
     * too old, not working!
     */
    private function parsePrepared($requestPath, $requestMethod) {
        $routing = $this->routes;
        $pieces = explode('/',$requestPath);
        $pieces = array_filter($pieces,function($piece){
            return (trim($piece)!='');
        });
        $base = array_shift($pieces);
        $route = isset($routing[$base]) ? $routing[$base] : [];
        if (empty($route)){
            array_unshift($pieces,$base);
            $base='';
            $route = isset($routing[$base]) ? $routing[$base] : [];
        }
        if ($route){
            $paramsPeases = [];
            foreach ($pieces as $key=>$piece)
            {
                if (!empty($route['sub'][$piece])){
                    $route=$route['sub'][$piece];
                }else{
                    $paramsPeases[]=$piece;
                }
            }
            $currentRoute = isset( $route[ count($paramsPeases) ] ) ? $route[ count($paramsPeases) ] : [] ;
            if (!empty($currentRoute)){     
                $controllerAction = $currentRoute['controller'];
                foreach ($paramsPeases as $key=>$piece)
                {
                    $controllerAction = str_replace('<param'.($key).'>',$piece,$controllerAction); 
                }
                $controllerAction = $this->parseClassController($controllerAction,$requestMethod);
                
                $params = [];
                if ( !empty($currentRoute['params']) ){
                    foreach ($currentRoute['params'] as $key=>$name)
                    {
                        $params[$name]=$paramsPeases[$key];
                    } 
                }
                return ['action'=>$controllerAction['action'], 'controller'=>$controllerAction['controller'], 'params'=>$params];        
            }
        }
        return ['action'=>$this->defaultController['action'],'controller'=>$this->defaultController['controller'],'params'=>[]];          
    }
    
    public function getControllerAction( $controllerName, $actionName, $requestMethod, $precontrollers=null, $data = [], $lastTry = false )
    {
        if (empty($precontrollers) && is_array($this->defaultPrecontrollers))
        {
            $precontrollers = $this->defaultPrecontrollers;
        }
        if ($this->precontroller && is_array($precontrollers))
        {
            $this->precontroller->import($data);
            $precontrollerClass = $this->precontroller;
            array_walk($precontrollers, function($precontroller) use ($precontrollerClass){
                $precontrollerClass->$precontroller();
            });
            $data = $precontrollerClass->export();
        }
        else if (!empty($data))
        {
            $data = $data;
        }
        
        $defaultController = ($requestMethod=='options' && $this->defaultOptionsController) ? $this->defaultOptionsController : $this->defaultController;
        if (!empty($controllerName))
        {
            if ($controllerName =='__ZBINLINECONTROLLER__')
            {
                 $action = $this->inlineRoutes[$actionName];
                 $Controller = new Blank();
            }
            else
            {
                $controllerFile = $this->path . $controllerName.'.php';
                if (!is_file($controllerFile))
                {
                    if (!$lastTry && $defaultController)
                    {
                        return $this->getControllerAction($defaultController['controller'], $defaultController['action'], $requestMethod, null, $data, true);
                    }
                    else
                    {
                        throw new \Exception('The specified controller class does not exist: ' . $this->path . $controllerName.'.php');
                    }
                }
                require $controllerFile;
                if (strrpos($controllerName,'/')!==false)
                {
                    $controllerName = substr($controllerName, strrpos($controllerName,'/')+1 );
                }
                $Controller = new $controllerName();
            }
            array_walk($data, function($value, $key) use ($Controller){
                $Controller->$key = $value;
            });
            if (method_exists($Controller,$actionName))
            {
                return [$Controller, $actionName];
            }
            if ( isset($action) && $action instanceof \Closure)
            {
                return $action->bindTo($Controller, $Controller);
            }
            if (!$lastTry && $defaultController)
            {
                return $this->getControllerAction($defaultController['controller'], $defaultController['action'], $requestMethod, null, $data, true);
            }
            else
            {
                throw new \Exception('A specified method of a controller class does not exist: ' . $controllerName . ':' . $actionName);
            }
        }
        $controllerFile = $this->path . $actionName;
        if (!is_file($controllerFile))
        {
            if (!$lastTry && $defaultController)
            {
                return $this->getControllerAction($defaultController['controller'], $defaultController['action'], $requestMethod, null, $data, true);
            }
            else
            {
                throw new \Exception('Specified action-file does not exist: '.$controllerFile);
            }
        }
        $controller = call_user_func( function($controllerFile, $data)
        {
            extract($data);
            return $controller = require $controllerFile;
        }, $controllerFile, $data);
        return $controller;
    }
    
    
    /**
     * Прорверка совпадения с шаблоном
     * @param string $path проверяемый uri-путь
     * @param string $pattern шаблон
     * @return array|false
     */
    public static function match($path, $pattern) {
        $patternArr = explode('/', trim($pattern, '/'));
        $pathArr = explode('/', trim($path, '/'));
        $length = count($patternArr);
        
        if ($length !== count($pathArr)) {
            return false;
        }
        
        $matches = [];
        for ($i = 0; $i < $length; ++$i) {
            if(strpos($patternArr[$i], '$')!==false && strpos($patternArr[$i],'[')){
                preg_match('/(.+)*\[([0-9]*)([-]?)([0-9]*)\]/', $patternArr[$i], $m);
                $pathPartLength = strlen($pathArr[$i]);
                if ( isset( $m[3] ) && isset( $m[4] ) && $m[3]=='-' && $m[4] > $m[2] ){var_dump(1);
                    if ( $pathPartLength >= $m[2] && $pathPartLength <= $m[4] ){
                        $matches[substr($m[1], 1)] = $pathArr[$i];
                    }else{
                        return false;
                    }
                } else if ( isset( $m[2] ) && $m[2] == $pathPartLength) {
                    $matches[substr($m[1], 1)] = $pathArr[$i];
                }else{
                    return false;
                }
            } else if (strpos($patternArr[$i], '$') === 0) {
                $matches[substr($patternArr[$i], 1)] = $pathArr[$i];
            } elseif ($patternArr[$i] !== $pathArr[$i]) {
                return false;
            }
        }
        return $matches;
    }
    
    /**
     * Подстановка переменных в строку-шаблон
     * @param string $template
     * @param array $params
     * @return string
     */
    public static function replace($template, array $params) {
        $replaces = [];
        array_walk($params, function($value,$name) use (&$replaces) {
            $replaces['$'.$name] = $value;
        });

        return strtr($template, $replaces);
    }
    /**
     * @deprecated
     */
    public function parse($path, $requestMethod) {
        return $this->getRouteData($path, $requestMethod);
    }
    /**
     * Поиск подходящего маршрута
     * @param string $path
     * @param array $params
     * @return array|false
     */
    public function getRouteData($path, $requestMethod) {
        if ($this->isConfigPrepared){
           return $this->parsePrepared($path, $requestMethod);
        }
        $params = [];
        foreach ($this->routes as $pattern => $template) {
            if (is_string($template)) {
                $pattern = str_replace(' ','',$pattern);
                if (strpos($pattern,'@')!==false){
                    $patternMethod = substr($pattern, 0, strrpos($pattern,'@') );
                    if ($patternMethod!=$requestMethod){
                        continue;
                    }
                    $pattern = substr($pattern, strrpos($pattern,'@')+1 );
                    $pattern = $pattern ? $pattern : '';
                }
                $template = str_replace(' ','',$template);
                $precontrollers = explode("->", $template);
                array_pop($precontrollers);
                if (strrpos($template,'->')!==false){
                    $template = substr($template, strrpos($template,'->')+2 );
                }
                $matches = self::match($path, $pattern);
                if ($matches !== false) {
                    $params += $matches;
                    $controllerAction =  $this->parseClassController( self::replace($template, $params), $requestMethod );
                    return ['action'=>$controllerAction['action'], 'controller'=>$controllerAction['controller'],  'precontrollers' => $precontrollers, 'params'=>$params];
                }
            }
        }
        return false;
    }
    
    public function processRequest(Request $Request, $userData=[])
    {
        $data = $this->getRouteData($Request->path(), $Request->method());
        $Request->defineRoute($data['controller'], $data['action'], $data['params']);
        $controller = $this->getControllerAction( $Request->controller(), $Request->action(), $Request->method(), $data['precontrollers'], $userData );
        return $controller;
    }
    
    public function addRoute($name, $path, $action, array $precontrollers=[])
    {
        $precontrollersString = implode('->',$precontrollers);
        $this->routes[$path] = (!empty($precontrollersString) ? $precontrollersString.'->':''). '__ZBINLINECONTROLLER__:'.$name;
        $this->inlineRoutes[$name] = $action;
    }
}
class Blank{}