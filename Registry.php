<?php

namespace ZB;

class Registry {
       
    private static $data = [];
    private static $writeable = [];
    private static $staticMode = false;
    private static $instance;
    public function __construct(array $data=null)
    {
        if (!is_null(self::$instance)){
            throw new \Exception('There can be only one Registry object');
        }
        if (!is_null($data)){
            self::$data = $data;
        }
        self::$instance = $this;
    }
    public static function get()
    {
        if (!self::$staticMode){
            throw new \Exception('Registry not allow static-mode usage');
        }
        if (is_null(self::$instance)){
            self::$instance = new self();
        }
        return self::$instance;
    }
    public function __get($key){
        if ( isset( self::$data[ $key ] ) )
        {
            return self::$data[ $key ];
        }
        return false;
    }
    
    public function set($key, $var){
        if ( ! isset( self::$data[ $key ] ) )
        {
            self::$data[$key] = $var;
        }
    }
    
    public function setWriteable($key,$var){
        if ( ! isset( self::$data[ $key ] ) || isset( self::$writeable[ $key ]) )
        {
            self::$data[$key] = $var;
            self::$writeable[$key] = 1;
        }
    }
    
    public function allowStaticMode(){
        self::$staticMode = true;
    } 
    
}